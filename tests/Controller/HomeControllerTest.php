<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{

    private $client;

    public function setUp(): void
    {
        $this->client = static::createClient();

    }

    public function testVisitingPageAvecLoggedIn()
    {

// get or create the user somehow (e.g. creating some users only
// for tests while loading the test fixtures)
        $userRepository = static::$container->get(UserRepository::class);
        $testUser       = $userRepository->findOneByEmail('mail@mail.com');

        $this->client->loginUser($testUser);

        $this->client->request('GET', '/home');
        $this->assertResponseIsSuccessful();
        //$this->assertSelectorTextContains('h1', 'Hello HomeController');
    }

    public function testNoUserRedirectionPage()
    {
        $this->client->request('GET', '/home');
        $this->assertResponseStatusCodeSame(302);
    }

    public function testNoUserRedirectionVersPageConnexion()
    {
        $this->client->request('GET', '/home');
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $location = $response->headers->get('location');
        $this->assertEquals('/login', $location);
    }
}
