<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testEntity(): void
    {
        $user=New User();
        $this->assertEquals(null,$user->getId());
        $user->setEmail('aa@bb.cc');
        $this->assertEquals('aa@bb.cc',$user->getUserIdentifier());
        $this->assertEquals('aa@bb.cc',$user->getEmail());
        $user->setRoles(['LE_ROLE']);
        $this->assertEquals(['LE_ROLE','ROLE_USER'],$user->getRoles());
        $user->setPassword('Password');
        $this->assertEquals('Password',$user->getPassword());
    }
}
